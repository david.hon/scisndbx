#!/bin/bash
apt install cmake curl g++
git clone --recursive https://github.com/gnudatalanguage/gdl
pushd gdl
./scripts/build_gdl.sh prep   # need to be sudo to add set of official packages
./scripts/build_gdl.sh configure
./scripts/build_gdl.sh build
./scripts/build_gdl.sh check

