     program tabulate
     ! gfortran -v -c -ffree-form tabulate.f
     ! https://fortran-lang.org/en/learn/building_programs/linking_pieces/
     use user_functions

     implicit none
     real    :: x, xbegin, xend
     integer :: i, steps
     write(*,*) Please enter the range (begin, end) and iteration steps
     read(*,*)  xbegin, xend, steps
     do i = 0, steps
        x = xbegin + i * (xend - xbegin) / steps
        write(*,'(2f10.4)') x, f(x)
     end do
     end program tabulate

