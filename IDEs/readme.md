# Curated lists of Python IDEs

## Focus on Vim and Emacs-text-mode in Cloud9 

    Script or scripts to configure/install vim and emacs (1 or more files)

    Instructions (1 file)

## https://www.cyberciti.biz/faq/how-to-install-vim-on-centos-8-using-dnf-yum/

    sudo yum install vim-enhanced emacs

    echo "alias vi='vim'" >> ~/.bashrc && source ~/.bashrc

    ./vimrc.sh && ./init.el.sh

## Emacs: https://wikemacs.org/wiki/Python

    https://lucidmanager.org/productivity/more-productive-with-emacs/

    https://www.emacswiki.org/emacs/PythonProgrammingInEmacs

## NeoVim https://github.com/neovim/pynvim.git

    https://neovim.io/roadmap/        

## Refs:

    https://hackr.io/blog/best-python-ide

    https://www.knowledgehut.com/blog/programming/best-python-ides-code-editors-guide

    https://www.makeuseof.com/python-ides-open-source-top/

## Eric: https://eric-ide.python-projects.org/index.html

## GDB Dash via python-dbg is not yet a full IDE: https://github.com/cyrus-and/gdb-dashboard.git 

    https://github.com/cyrus-and/gdb-dashboard/wiki
    https://wiki.python.org/moin/DebuggingWithGdb
    sudo yum install yum-utils
    sudo debuginfo-install glibc
    sudo yum install gdb python-debuginfo

    https://www.rojtberg.net/2630/debugging-python-with-gdb-on-ubuntu/
    sudo apt install python3.10-dbg

## https://github.com/spacetelescope/jdaviz.git -- JWST dta visualization


## Ninja-IDE: git clone https://github.com/ninja-ide/ninja-ide.git

    ubuntu -- sudo apt install python3-python-qt-binding
    rhel -- sudo yum install PyQt5
    pip3 install PyQt5
    git clone --depth 1 http://github.com/ninja-ide/ninja-ide.git 
    pushd ninja-ide
    pip3 install -r requirements.txt
    python3 ninja-ide.py

## Pycharm Jetbrains: https://www.simplilearn.com/tutorials/python-tutorial/pycharm

## Pydev for Eclipse: https://www.pydev.org/

## VScodium Python plugin: https://opensource.com/article/22/11/python-vs-code-codium

## Sublime Text Python plugin: https://realpython.com/setting-up-sublime-text-3-for-full-stack-python-development/

## Spyder: https://github.com/spyder-ide/spyder.git

## Thonny: https://realpython.com/python-thonny/

## Brackets python extension? https://brackets.io/  ...  https://github.com/brackets-cont/brackets.git

## Vim: https://realpython.com/vim-and-python-a-match-made-in-heaven/

---

# Online Python IDEs

## Cocalc: https://cocalc.com/features/python

## Google Colab: https://colab.research.google.com/

## Jupyter: https://www.techrepublic.com/article/google-colab-vs-jupyter-notebook/

## PythonAnywhere: https://www.pythonanywhere.com/

