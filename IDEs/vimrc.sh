#!/bin/bash
# from https://realpython.com/vim-and-python-a-match-made-in-heaven/ ... python within vim
# Writes a sample .vimrc to stdout. Create/init -- ./vimrc.sh > ~/.vimrc
cat << EOF
:python import sys; print(sys.version)

https://realpython.com/vim-and-python-a-match-made-in-heaven/
" set up Vundle in your .vimrc by adding the following to the top of the file:

set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

" ...

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
That’s it. You’re now set up to use Vundle. Afterward, you can add the plugins you want to install, then fire up VIM and run:

:PluginInstall
Open a file with :sp <filename>, you split the layout vertically (opening the new file below the current file).
EOF

