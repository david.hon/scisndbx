#!/bin/bash
echo mkdir -p ~/miniconda3
echo wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
echo bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
echo rm -rf ~/miniconda3/miniconda.sh
echo ~/miniconda3/bin/conda init bash
echo ~/miniconda3/bin/conda init zsh
