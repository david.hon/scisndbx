#!/usr/bin/env python3
import boto3

_AWS_REGION = "us-east-2"

def ec2launch(tmplat=None):
  """
  TBD instance templates: AMI, Firewall/SecurityGroup, StorageVolume, ...
  https://us-east-1.console.aws.amazon.com/ec2/v2/home?region=us-east-1#LaunchInstanceFromTemplate:
  """
  try: pass
  except: return None
  return tmplat

def ec2stop(id='i-1234567890abcdef0'):
  """
  resp = boto3.client.stop_instances(InstanceIds=[id,], Hibernate=False, DryRun=False, Force=True)
  Expected Output:
  {
    'StoppingInstances': [
        {
            'CurrentState': {
                'Code': 64,
                'Name': 'stopping',
            },
            'InstanceId': 'i-1234567890abcdef0',
            'PreviousState': {
                'Code': 16,
                'Name': 'running',
            },
        },
    ],
    'ResponseMetadata': {
        '...': '...',
    },
  }
  """
  resp = client.stop_instances(InstanceIds=[id,], Hibernate=False, DryRun=False, Force=True)
  print(resp)
  return resp

def volresize():
  """
  response = boto3.client.modify_volume(
    DryRun=True|False,
    VolumeId='string',
    Size=123,
    VolumeType='standard'|'io1'|'io2'|'gp2'|'sc1'|'st1'|'gp3',
    Iops=123,
    Throughput=123,
    MultiAttachEnabled=True|False
  )
  """
  return

def vollist(region=_AWS_REGION):
  ec2_resource = boto3.resource('ec2', region_name=_AWS_REGION)
  allvols = ec2.volumes.all()
  for vol in allvols: print(vol)
  return allvols

def main(args={}):
  """
  from https://github.com/boto/boto3 readme
  bash
  ython3 -m venv py3env
  . py3env/bin/activate
  python -m pip install boto3
  """
  ec2 = boto3.client('ec2', region_name=_AWS_REGION)
  resp = ec2.describe_instances()
  print(resp)
  resp = vollist(_AWS_REGION)
  #resp = ec2stop()
  return resp

if __name__ == "__main__":
  import inspect
  print(inspect.getdoc(main))
  print(inspect.signature(main))
  main()

