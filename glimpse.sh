#!/bin/bash
echo https://manpages.ubuntu.com/manpages/jammy/man1/glimpseindex.1.html
folder=$(pwd)
echo $folder
echo glimpseindex -b $folder
gis_folder=~/hon_gitlab/opengis
echo glimpseindex -b $gis_folder
cmr_folder=~/hon_gitlab/cmr_k8s_k3s
echo glimpseindex -b $cmr_folder
echo ls -alqhF ~/.glimpse
echo glimpse foobar
