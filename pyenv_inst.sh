#!/bin/bash
echo intall python releases along with pyenv and/or pyvenv and/or virtualenv
echo
echo Given fortran / gfortran is used heavily by NOAA scientists ...
echo pip install -U fortran-magic
echo
echo python distros
echo https://www.python.org/downloads/
echo https://www.python.org/downloads/release/python-3112/
echo https://www.anaconda.com/products/distribution
echo https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
echo https://www.jython.org/download

echo
echo https://realpython.com/python-virtual-environments-a-primer/ -- mentions virtualenv and conda / miniconda
echo
echo pyenv is perhaps the best choice ... allows install of many release/versions of CPython
echo
echo 'https://realpython.com/lessons/pyenv-install-python'
echo
echo 'curl https://pyenv.run | bash'
echo
echo 'pyenv install --list | grep "jython"'
echo
echo https://github.com/pyenv/pyenv-installer.git
echo
echo https://realpython.com/intro-to-pyenv/ -- recommends https://github.com/pyenv/pyenv-installer

echo
echo python IDEs
#echo https://code.visualstudio.com/
echo
echo https://www.online-python.com/
echo
echo https://realpython.com/emacs-the-best-python-editor/
echo
echo https://realpython.com/vim-and-python-a-match-made-in-heaven/
echo
echo https://vscode.pro/
echo
echo open source release of mswindows vscode
echo
echo https://vscodium.com/
echo
echo 'wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg'
echo
echo "echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list"
echo
echo 'sudo apt update && sudo apt install codium'
echo
echo https://codesolid.com/python-docker-examples-sagemath-in-a-container/
echo

echo 'https://realpython.com/intro-to-pyenv' -- perhaps has the most to offer
echo
echo ubuntu deps pyenv ...
echo
echo 'sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl'

echo
echo centos deps pyenv ...
echo 'sudo yum install -y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel xz xz-devel libffi-devel'
echo
echo curl https://pyenv.run | bash
echo
echo 'export PATH="$HOME/.pyenv/bin:$PATH"'
echo
echo 'eval "$(pyenv init -)"'
echo
echo 'eval "$(pyenv virtualenv-init -)"'

echo
echo pip3 install pyvenv
echo
echo 'pyenv install --list'
echo
echo python3 -m venv py3venv
echo
echo python3 -m venv py3venv --system-site-packages
echo
echo pyvenv --help

