#!/bin/bash
echo 'dbhon cloud9 aws console'
echo https://us-east-1.console.aws.amazon.com/cloud9/ide/e975033d8cce44939d6d774b819b9e58

echo
echo https://github.com/docker-library/python/
echo https://github.com/docker-library/python/tree/master/3.11
echo pyenv_inst.sh:echo https://codesolid.com/python-docker-examples-sagemath-in-a-container/

echo
echo https://www.bananatronics.org/how-to-move-docker-volumes/
echo 'official python docker git repo'
echo 'https://github.com/docker-library/python'

echo 
echo 'consider alt bridge config'
echo https://docs.docker.com/network/bridge/

echo
echo 'move / change the location of the docker runtime storage volume'
echo 'https://access.redhat.com/sites/default/files/attachments/12052018_systemd_6.pdf'
echo sudo systemctl stop docker
echo sudo systemctl status docker
echo 'ps faux | grep -i docker'
echo sudo 'touch /etc/docker/daemon.json && echo "'"{ "data-root": "/vol20g/docker", "dns": ["8.8.8.8","8.8.4.4"] }"'" >> /etc/docker/daemon.json'
echo sudo systemctl reload docker
echo sudo systemctl daemon-reload
echo sudo service docker start

#echo another ref
#echo https://www.digitalocean.com/community/questions/how-to-move-the-default-var-lib-docker-to-another-directory-for-docker-on-linux
#echo mkdir /home/docker
#echo rsync -avxP /var/lib/docker/ /home/docker
#echo sudo vim /lib/systemd/system/docker.service
#echo ExecStart=/usr/bin/dockerd -g /home/docker -H fd:// --containerd=/run/containerd/containerd.sock
#echo sudo systemctl daemon-reload
#echo docker inspect image_id | grep WorkDir

