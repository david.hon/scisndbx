#!/bin/bash
echo https://us-east-1.console.aws.amazon.com/cloud9/ide/e975033d8cce44939d6d774b819b9e58
echo https://aws.amazon.com/blogs/compute/how-to-mount-linux-volume-and-keep-mount-point-consistency/
echo https://docs.aws.amazon.com/cloud9/latest/user-guide/move-environment.html#move-environment-resize



echo 'size option units in MB...'
echo aws ec2 modify-volume --region us-east-1 --volume-id vol-11111111111111111 --size 2000 --volume-type gp2 --iops 100
echo 'dmesg|grep -i uuid'
echo 'cat /etc/fstab'
echo blkid
echo file -s /dev/nvm*
echo file -s /dev/nvme1n1
echo mkfs.xfs /dev/nvme1n1
echo xfs_growfs /dev/nvme1n1
echo mkdir /vol20g
echo '"$(blkid -o export /dev/nvme1n1 | grep ^UUID=) /vol20g xfs defaults,noatime" | tee -a /etc/fstab'
echo mount -a
echo df -h


echo 'https://jtway.co/resize-amazon-ebs-volumes-without-a-reboot-ca118b010b44'
echo from above volumes.tf ... create a 1G vol

echo create new ebs vol
cat <<TFTXT0
tftxt0='resource "aws_ebs_volume" "devtest" {
  availability_zone = "us-east-1a"
  size = 1000
  type = "gp2"
  tags {
    Name      = "devtest"
    Role      = "dev"
    Terraform = "true"
    FS        = "xfs"
  }
}'
TFTXT0

echo find the new vol Id ... below example is from jtway blog...
echo volId='vol-0123456789abcdef0'
echo "terraform import aws_ebs_volume.devtest $volId"
echo 'terraform plan -target=aws_ebs_volume.devtest'
echo "edit volumes.tf for 2G resize"

cat <<TFTXT1
tftxt1='resource "aws_ebs_volume" "devtest" {
  availability_zone = "us-east-1a"
  size = 2000
  type = "gp2"
  tags {
    Name      = "devtest"
    Role      = "dev"
    Terraform = "true"
    FS        = "xfs"
  }
}'
TFTXT1

echo 'terraform apply -target=aws_ebs_volume.mysql ~ aws_ebs_volume.devtest size: "1000" => "2000"'

