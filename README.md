# scisndbx

## TODO -- Test Python plugin/extensions for Vim and Emacs in and out of the Cloud9 envs. Also try latest supermin...

miniconda and pyenv are perhaps the best choices ...

https://waylonwalker.com/install-miniconda -- provides handy bash script for install. Evidently Python 3.10 is the latest supported (mini)conda release.

    mkdir -p ~/miniconda3
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
    bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
    rm -rf ~/miniconda3/miniconda.sh
    ~/miniconda3/bin/conda init bash
    ~/miniconda3/bin/conda init zsh


miniconda provides the conda pkg tool which can be invoked to install all anaconda python supported modules. 

pyenv allows install of many releases / versions of CPython -- latest is 3.11.x -- and other Pythons like Jython

    curl https://pyenv.run | bash
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
    echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(pyenv init -)"' >> ~/.bashrc

While "pyenv --install" allegedly supports Anaconda installations, some issues have been reported over time...

https://devpress.csdn.net/python/63045a20c67703293080babd.html

According to the above post:

    It turns out the best solution is to install conda, pyenv separately, and manage their virtual environments separately as well.

https://github.com/pyenv/pyenv/wiki#suggested-build-environment -- lists python build pkgs for various OS that are pre-reqs for "pyenv --install"`

https://docs.conda.io/en/latest/miniconda.html -- provides some pyhon modules and "conda install" to add more modules, above and beyond full anaconda

Attractive option for collaboration include Jupyter and Cocalc -- https://datasciencenotebook.org/jupyter-realtime-collaboration and https://cocalc.com/software/python/22.04

https://git.mysmce.com/heliocloud/cloud-tutorials -- note jupyter notebooks

Note newer Jupyter support of non-python runtimes aka kernels. Also note IPython is available standalone: https://ipython.org/install.html

## Intro

Execute Bash script ./pyenv_inst.sh to install envs. 

Note the impressive set of environments provided by 'pyenv install --list'

Also note the excellent python integration with GDB -- gfortran, C, C++, etc.

TBD: try gfortran python-gdb and compare to c/c++

https://numpy.org/doc/stable/f2py/f2py-examples.html

https://fortran-lang.org/en/learn/building_programs/compiling_source/

https://notebook.community/mgaitan/fortran_magic/documentation

https://lfortran.org/blog/ -- gfortran repl?

https://docs.lfortran.org/en/installation/ -- most deps automagically provided by anaconda ... conda virtual env install:

    conda create -n lf
    conda activate lf
    conda install lfortran -c conda-forge
    conda install jupyter -c conda-forge
    jupyter notebook ... and selecting New->Fortran.

https://numpy.org/doc/stable/f2py/ -- mayhap worth a look?

    python -m numpy.f2py

https://en.wikibooks.org/wiki/Fortran/Documenting_Fortran

## Refs

https://numfocus.org/sponsored-projects -- quite a few python projects

https://realpython.com/python-package-quality/

https://www.frontiersin.org/articles/10.3389/fspas.2022.1006839/full -- Profiling heliophysics and space physics data in the pythonic cloud focuses on Python support for the most-used formats of CDF, FITS, and NetCDF4/HDF

https://github.com/sunpy/sunpy.git ... and ... https://github.com/astropy/astroquery.git 

https://realpython.com/lessons/pyenv-install-python

https://www.educative.io/answers/anaconda-vs-miniconda

https://ipython.org

https://towardsdatascience.com/get-your-computer-ready-for-machine-learning-how-what-and-why-you-should-use-anaconda-miniconda-d213444f36d6

https://undo.io/resources/gdb-watchpoint/python-gdb/

https://gitlab.com/davidbhon/sandbox.git -- somewhat dated repo that has a python-gdb snapshot ... TBD freshen for 2023

https://onecompiler.com/python

https://pypi.org/

https://realpython.com/

https://github.com/gvanrossum

https://cocalc.com/features

https://vscodium.com/

https://opensource.com/article/22/11/python-vs-code-codium -- "a good, general-purpose, open source code editor with Python integration"

https://www.sagemath.org/ and  https://wiki.sagemath.org/IDE

## Output of source ./pyenv_inst.sh 

intall python releases along with pyenv and/or pyvenv and/or virtualenv

### python distros

https://www.python.org/downloads/

https://www.python.org/downloads/release/python-3112/

https://www.anaconda.com/products/distribution

https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

https://www.jython.org/download

https://realpython.com/python-virtual-environments-a-primer/ -- mentions virtualenv and conda / miniconda

https://realpython.com/lessons/pyenv-install-python

pyenv install --list | grep "jython"

https://github.com/pyenv/pyenv-installer.git

https://realpython.com/intro-to-pyenv/ -- recommends https://github.com/pyenv/pyenv-installer


### python IDEs

https://www.online-python.com/

https://realpython.com/emacs-the-best-python-editor/

https://realpython.com/vim-and-python-a-match-made-in-heaven/

https://vscode.pro/

open source release of mswindows vscode

https://vscodium.com/

wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg

echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list

sudo apt update && sudo apt install codium

https://www.docker.com/blog/how-to-dockerize-your-python-applications/

https://docs.docker.com/language/python/build-images/

https://github.com/docker-library/python.git

https://codesolid.com/python-docker-examples-sagemath-in-a-container/

https://realpython.com/intro-to-pyenv -- perhaps has the most to offer

### ubuntu deps pyenv ...

sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl

centos deps pyenv ...

sudo yum install -y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel xz xz-devel libffi-devel

/bin/bash

Usage: curl https://pyenv.run | bash

For more info, visit: https://github.com/pyenv/pyenv-installer


index_main() {

    set -e

    curl -s -S -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash

}

index_main

export PATH="$HOME/.pyenv/bin:$PATH"

eval "$(pyenv init -)"

eval "$(pyenv virtualenv-init -)"

pip3 install pyvenv

pyenv install --list

python3 -m venv py3venv

python3 -m venv py3venv --system-site-packages

pyvenv --help


# Preserve initially generated Gitlab Readme

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/david.hon/scisndbx.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/david.hon/scisndbx/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
